﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_Enemy : MonoBehaviour
{
    public static float enemyVelocity;
    private Rigidbody rb;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    void Update()
    {
        //hace que los enemigos se muevan hacia la izquierda
        rb.AddForce(new Vector3(0, 0, -enemyVelocity), ForceMode.Force);
        OutOfBounds();
    }

    //hace que el gameObject asignado, se destruya cuando su posicion sea menor a 15, es decir no se vea en pantalla
    public void OutOfBounds()
    {
        if (this.transform.position.z <= -28)
        {
            Destroy(this.gameObject);
        }
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            Destroy(collision.gameObject);
            Time.timeScale = 0;
        }


    }
}
