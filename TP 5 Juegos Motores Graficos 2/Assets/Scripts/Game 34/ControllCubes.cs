﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControllCubes : MonoBehaviour
{
    public Material[] material;
    Renderer rend;
    public float vel;
    private Rigidbody rb;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        rend = GetComponent<Renderer>();
        rend.enabled = true;

    }

    // Update is called once per frame
    void Update()
    {
        if(rb.useGravity == true)
        {
            rend.sharedMaterial = material[1];
        }
        else
        {
            rb.velocity = new Vector3(0, 1 * vel, 0);
            rend.sharedMaterial = material[0];
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Floor"))
        {
            rb.velocity = new Vector3(0, 1 * vel, 0);

            rb.useGravity = false;
        }
    }
}
