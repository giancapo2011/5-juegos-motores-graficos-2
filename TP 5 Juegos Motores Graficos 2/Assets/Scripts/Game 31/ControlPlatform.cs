﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlPlatform : MonoBehaviour
{
    private Rigidbody rb;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.D))
        {
            transform.Rotate(new Vector3(0, 0, -200 * Time.deltaTime));
        }
        //else
        //{
        //    transform.Rotate(new Vector3(0, 0, 0));
        //}
        if (Input.GetKey(KeyCode.A))
        {
            transform.Rotate(new Vector3(0, 0, 200 * Time.deltaTime));
        }
        //else
        //{
        //    transform.Rotate(new Vector3(0, 0, 0));
        //}
    }
}
