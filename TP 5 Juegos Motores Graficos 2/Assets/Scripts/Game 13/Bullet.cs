﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public GameObject Bala;

    // Start is called before the first frame update
    void Start()
    {
        transform.Rotate(new Vector3(0, -90, 0));
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("BallRotate"))
        {
            Bala.transform.parent = GameObject.Find("Sphere").transform;

        }

        if (collision.gameObject.CompareTag("Bullet"))
        {
            Destroy(gameObject);
           

        }
    }

}
